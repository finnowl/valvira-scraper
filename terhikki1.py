﻿#TODO: scenario 4 basically needs to loop through scenarios 2 and 3 again, including submitting the captcha again
#TODO: write a function of the captcha so that it checks first if captcha needs to be filled in


from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import re
import time
import csv
import os

#custom-made function in separate file
from captcha import answer_captcha




#settings
environment = 'mac' #osx or pc
csvInput = False
csvInputFile = 'input_data.csv'
csvOutput = True
csvOutputFile = 'output_data.csv'
csv.register_dialect('myDialect', quoting=csv.QUOTE_ALL, skipinitialspace=True)

#input data variable 
namelist = []

#temp variable to check loops
finalResults = []

#results data variable in format: [[lookup_firstname, lookup_surname], [found_firstname, found_surname], born_year, [titles]]
finalResultsList = []




#csv input
if csvInput:
	with open(csvInputFile, 'r') as infile:
		reader = csv.reader(infile, dialect='myDialect')
		namelist = list(reader)
	infile.close()

else:
	#namelist = [['Anttsdsdfi', 'Mikkonen'], ['Seppo', 'Laaksonen'], ['Antti', 'Kinnunen'], ['Antti', 'Mikkonen']]
	namelist = [['Antti', 'Mikkonen']]




#custom function for splitting title list in scenario 3
#splits correctly eg. 'erikoislääkäri  korva-, nenä- ja kurkkutaudit, kirurgi' as it's problematic with comma
def splitify(titles):
	newList = re.split(r'(?<!-),', titles) #splits only if comma is NOT preceded by a dash
	newList = [item.strip() for item in newList] #strip trailing and leading whitespaces
	newList = [re.sub(' +', ' ',item) for item in newList] #strip duplicated whitespaces
	return newList


###THE SCRAPER

#loop through all people in namelist
for x in namelist:

  #pick out first and surname
  firstname = x[0]
  surname = x[1]

  #print what's going on
  print('Looking up %s %s ...' % (firstname, surname))

  #set options for browser
  options = Options()
  options.headless = True

  if environment == 'mac':
    path = '/Users/uge/Documents/pythongrejer/really-old-tests/Webdriver/chromedriver' #set correct version of driver

  if environment == 'pc':  
    path = 'D:/chromedriver75/chromedriver.exe' #set correct version of driver
    options.binary_location = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe'

  #open new browser
  driver = webdriver.Chrome(options = options, executable_path = path)
  driver.implicitly_wait(10) #poll the DOM for certain time when trying to find any element not immediately available
  
  #open page in browser
  driver.get('https://julkiterhikki.valvira.fi/')
  
  #fill in form with first name and surname 
  inputElement1 = driver.find_element_by_id('inputName')
  inputElement1.send_keys(firstname)
  inputElement2 = driver.find_element_by_id('inputSurname')
  inputElement2.send_keys(surname)
  driver.find_element_by_class_name('btn').click()

  #give the page some time to load
  time.sleep(1)

  #extract captcha question
  labelList = driver.find_elements_by_class_name('control-label')
  question = labelList[3].get_attribute('innerHTML').encode('utf-8') #4th label is the question
  
  #get answer to captcha question
  answer = str(answer_captcha(question))

  #fill in answer in form element
  inputElement = driver.find_element_by_id("captchaInput")
  inputElement.send_keys(answer)

  #submit answer, try in case wrong answer gives wrong session key
  try:
    driver.find_elements_by_class_name('btn')[2].click()
  except:
    pass

  #give the page some time to load
  time.sleep(1)

  #check whether the answer was correct
  try:
    captchaFeedback = driver.find_element_by_id("captchaHelp").get_attribute('innerHTML')

    if 'Vastaus on' in captchaFeedback:
      print('wrong answer: You replied "%s" to the question "%s"' % (answer, question))
      driver.close()
      exit()

  except:
    pass

  #sleep while the results are loading
  time.sleep(1)

  #create variables for extraction and cleaning
  cleanTitles = []

  amountButtons = driver.find_elements_by_class_name('btn')
  amountH2 = driver.find_elements_by_tag_name('h2')

  isNoResults = False
  isOneResult = False

  #check content of h2 tags for scenario 1 och 2 below
  for tag in amountH2:
    if "Ei hakutuloksia" in tag.get_attribute('innerHTML'):
      isNoResults = True
    elif "Hakutulos" in tag.get_attribute('innerHTML'):
      isOneResult = True




	##SCENARIO 1: no results 
	#contains 2 buttons and h2 tag contains "Ei hakutuloksia"
  if len(amountButtons) == 2 and isNoResults:
    print('   No result')
    finalResultsList.append([[firstname, surname], 'no match', None, None])


  ##SCENARIO 2: One result (ex Seppo Laaksonen, Antti Laaksonen, Antti Huovinen)
	#page contains 2 buttons and h2 tag contains "Hakutulos"
  #there are 3 buttons in this scenario but one of them is hidden and useless
  elif len(amountButtons) == 2 and isOneResult:
    print('   1 result')

    table = driver.find_element_by_tag_name('table')
    tableCells = table.find_elements_by_tag_name('td')

    finalResultsList.append([[firstname, surname], tableCells[1].get_attribute('innerHTML').split('  '), tableCells[3].get_attribute('innerHTML'), [tag.get_attribute('innerHTML') for tag in tableCells[7].find_elements_by_tag_name('p')]])


	##SCENARIO 3: 2-5 results (Antti Kinnunen 5 results, Mikko Lehtinen 3 results)
	#contains 4-7 buttons
	#ex 7 buttons (if 5 results)
  elif 4 <= len(amountButtons) <= 7:
    print('   2-5 results')

    for button in amountButtons[2:]: #ignore two first buttons that are irrelevant
      #outerDivs = button.find_elements_by_tag_name('div')
      #innerDiveOne = outerDivs[0].find_elements_by_tag_name('div') #name
      #innerDiveTwo = outerDivs[1].find_elements_by_tag_name('div') #year born
      #innerDiveThree = outerDivs[2].find_elements_by_tag_name('div') #titles separated by commas
      spans = button.find_elements_by_tag_name('span')

      finalResultsList.append([[firstname, surname], spans[1].get_attribute('innerHTML').split('  '), spans[3].get_attribute('innerHTML'), splitify(spans[5].get_attribute('innerHTML'))])


	##SCENARIO 4: 6 or more results (Antti Mikkonen 6 results)
	#contains 8 or more buttons
	#ex contains 9 buttons (if 7 results)
	#ex contains 8 buttons (if 6 results)
	#requires another layer of scraping
  elif len(amountButtons) >= 8:

	  #get data from results
    rawTitles = driver.find_elements_by_class_name('btn')

	  #first two button's innerhtml are irrelevant, thus range starting from 2
    for x in range(2,len(rawTitles)):
      title = rawTitles[x].get_attribute('innerHTML')
      cleanTitles.append(title)

      finalResultsList.append([[firstname, surname], [None, None], None, cleanTitles])

  time.sleep(2)



#print results 
print('\n')
print(finalResultsList)
print('\n')



#write finalResultsList to csv
if csvOutput:

  finalResultsListToCSV = [['lookup_firstname', 'lookup_surname', 'found_firstname', 'found_surname', 'born_year', 'title(s)']]

  for item in finalResultsList:

    tempList = []

    tempList.append(item[0][0])
    tempList.append(item[0][1])
    tempList.append(item[1][0])
    tempList.append(item[1][1])
    tempList.append(item[2])
    tempList.append(', '.join([str(x) for x in item[3]]))

    finalResultsListToCSV.append(tempList)


  #check whether output file name exists
  if os.path.isfile(csvOutputFile):
    i = 1
    while os.path.isfile(csvOutputFile):
      csvOutputFile.replace('.csv', '')
      csvOutputFile = csvOutputFile + '_' + str(i) + '.csv'
      i += 1 


  with open(csvOutputFile, 'w', newline='') as outfile:
    writer = csv.writer(outfile, dialect='myDialect')
    for row in finalResultsListToCSV:
      writer.writerow(row)

  outfile.close()
