

def answer_captcha(captcha):

  number_as_text = [
  'yksi',
  'kaksi',
  'kolme',
  'neljä',
  'viisi',
  'kuusi',
  'seitsemän',
  'kahdeksan',
  'yhdeksän',
  'kymmenen',
  'yksitoista',
  'kaksitoista',
  'kolmetoista',
  'neljätoista',
  'viisitoista',
  'kuusitoista',
  'seitsemäntoista',
  'kahdeksantoista',
  'yhdeksäntoista',
  'kaksikymmentä'
  ]

  #riddle and answers
  riddles = [
  ['Mikä näistä on eläin: kissa, auto, talo?','kissa'],
  ['Mikä näistä on eläintarha: Korkeasaari, Isovuori, Pikkujärvi?','Korkeasaari'],
  ['Mikä näistä on valtio: takki, turkki, housut?','turkki'],
  ['Mikä on myös numero ja koristellaan jouluna (ei mänty)?','kuusi'],
  ['Mikä on Suomen pääkaupunki?','Helsinki'],
  ['Mikä vuodenaika tulee kesän jälkeen?','syksy'],
  ['Mitä näistä käytetään saunassa: pyörä, vasta, ehkä?','vasta'],
  ['Pikkurilli on sormi. Kyllä vai ei?','Kyllä'],
  ['Tietokoneen kanssa voidaan käyttää jotain näistä: kissa, koira, hiiri?','hiiri'],
  ['Mikä näistä on Suomen naapurimaa: Italia, Ruotsi, Kiina?','Ruotsi']
  ]


  #In case it is a mathematical question

  captcha = captcha.decode()

  if (captcha.startswith("Mitä on") == True) or (captcha.startswith("Paljonko on") == True):

    #split question into separate words and load into list
    question = captcha.split()

    first_no = question[2] #third word is the first number
    second_no = question[4][:-1] #fifth word is the second number, strip last full stop

    #test if question contains numbers
    if first_no.isdigit() == True:
      answer = int(first_no) + int(second_no)

    #test if question contains numbers as text
    else: 
      answer = (number_as_text.index(first_no)+1) + (number_as_text.index(second_no)+1)

    #test if answer should be given as a number  
    if "numerona" in question: 
      return(answer)

    #test if answer should be given as text
    else:
      return(number_as_text[answer-1])


  #In case it is a riddle question
  else:
    for x in range(0,len(riddles)):
      #look up riddle in list
      if captcha == riddles[x][0]:
        return(riddles[x][1])
      
    #if riddle can't be found in list
    return(' ')