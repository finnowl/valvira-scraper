# Scraper for the JulkiTerhikki database of healthcare professionals

A Selenium scraper for the Finnish version of a Javascript-based online database (https://julkiterhikki.valvira.fi/).

There are two types of security questions which the function in **captcha.py** takes care of:

1. simple algebra questions where the answer should be typed either with letters (in Finnish) or numbers
2. simple logical questions that seem to repeat often

The scraper looks up names of health care professionals, based on first and last names in **input_data.csv**. The output is also a CSV file.

The scraper was created and tested in June 2019, but it is still not complete for all scenarios.
